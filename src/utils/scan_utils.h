#pragma once
#include <types/defs.h>

namespace srrg_loam {

  inline void getStartEndOrientation(real& start, real& end,
                              const pcl::PointXYZ& start_point,
                              const pcl::PointXYZ& end_point) {       
    start = -std::atan2(start_point.y, start_point.x);
    end   = -std::atan2(end_point.y, end_point.x) + 2 * real(M_PI);
    if (end - start > 3 * M_PI) {
      end -= 2 * M_PI;
    } else if (end - start < M_PI) {
      end += 2 * M_PI;
    }
  }

  inline real squaredNorm(const pcl::PointXYZ& p) {
    return p.x*p.x + p.y*p.y + p.z*p.z;
  }
  

  template <typename PointT>
  inline real calcSquaredDiff(const PointT& a, const PointT& b, const real wb = 1.f) {
    const real diffX = a.x - b.x * wb;
    const real diffY = a.y - b.y * wb;
    const real diffZ = a.z - b.z * wb;
    return diffX * diffX + diffY * diffY + diffZ * diffZ;
  }

  template <typename PointT>
  inline real pointDistance(const PointT& p) {
    return std::sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
  }

  template <typename PointT>
  inline real squaredPointDistance(const PointT& p) {
    return p.x * p.x + p.y * p.y + p.z * p.z;
  }


}
