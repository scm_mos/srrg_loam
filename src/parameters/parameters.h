#pragma once
#include <types/defs.h>

namespace srrg_loam {

  struct DetectorConfig {
    DetectorConfig() {
      number_feature_region       = 6;
      curvature_region            = 5;
      max_corner_sharp            = 2; 
      max_corner_less_sharp       = 20;
      max_surface_flat            = 4; 
      less_flat_filter_size       = 0.2;
      surface_curvature_threshold = 0.1;
    }
    
    size_t number_feature_region;   // The number of regions used to distribute the feature extraction
    size_t curvature_region;        // The number of surrounding points (+/- region around a point)
                                    // used to calculate a point curvature.
    size_t max_corner_sharp;        // The maximum number of sharp corner points per feature region.
    size_t max_corner_less_sharp;   // The maximum number of less sharp corner points
                                    // per feature region.
    size_t max_surface_flat;        // The maximum number of flat surface points per feature region
    real less_flat_filter_size;     // The voxel size used for down sizing the remaining
                                    // less flat surface points
    real surface_curvature_threshold; //The curvature threshold below / above a point is
                                      // considered a flat / corner point.
  };


  struct RegistrationConfig {
    RegistrationConfig() {
      scan_period                 = 0.1;
      imu_history_size            = 200;
    }
    real scan_period;               // time per scan
    size_t imu_history_size;        // size of IMU history
  };


}
