#pragma once
#include <types/defs.h>
#include <parameters/parameters.h>

namespace srrg_loam {

  class Detector {
  public:
    Detector();
    virtual ~Detector();

    inline DetectorConfig& mutableConfig() {return _config;}
    inline const DetectorConfig& config() const {return _config;}

    inline void setLaserScans(CloudXYZIVector* laser_scans_) {_laser_scans = laser_scans_;}

    inline void setCornerClouds(CloudXYZI* corner_points_sharp_, CloudXYZI* corner_points_less_sharp_) {
      _corner_points_sharp = corner_points_sharp_;
      _corner_points_less_sharp = corner_points_less_sharp_;
    }
    inline void setSurfaceClouds(CloudXYZI* surface_points_flat_, CloudXYZI* surface_points_less_flat_) {
      _surface_points_flat = surface_points_flat_;
      _surface_points_less_flat = surface_points_less_flat_;
    }

    void setup();
    void compute();

  protected:
    void clearClouds();
    void collectFullResolutionCloud();
    void extractFeatures();
    void setScanBuffersFor(const size_t& start_idx_, const size_t& end_idx_);
    void markAsPicked(const size_t& cloud_idx_, const size_t& scan_idx_);
    void setRegionBuffersFor(const size_t& start_idx_, const size_t& end_idx_);

  private:
    DetectorConfig _config;

    CloudXYZI _laser_cloud;
    std::vector<IndexRange> _scan_indices;    ///< start and end indices of the individual scans within the full resolution cloud

    std::vector<real> _region_curvature;      ///< point curvature buffer
    std::vector<PointLabel> _region_label;    ///< point label buffer
    std::vector<size_t> _region_sort_indices; ///< sorted region indices based on point curvature
    std::vector<int> _scan_neighbor_picked;   ///< flag if neighboring point was already picked

    CloudXYZIVector* _laser_scans = 0;

    CloudXYZI* _corner_points_sharp = 0;       ///< sharp corner points cloud
    CloudXYZI* _corner_points_less_sharp = 0;  ///< less sharp corner points cloud
    CloudXYZI* _surface_points_flat = 0;       ///< flat surface points cloud
    CloudXYZI* _surface_points_less_flat = 0;  ///< less flat surface points cloud

    bool _is_init;
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace srrg_loam */
