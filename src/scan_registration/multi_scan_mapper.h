#pragma once
#include <types/defs.h>

namespace srrg_loam {

  class MultiScanMapper {
  public:
    MultiScanMapper(const real& lower_bound_,
                    const real& upper_bound,
                    const size_t& num_scan_rings_);

    virtual ~MultiScanMapper();

    inline const real& lowerBound() const {return _lower_bound;}
    inline const real& upperBound() const {return _upper_bound;}
    inline const size_t& numScanRings() const {return _num_scan_rings;}
    inline const real& factor() const {return _factor;}

    int getRingForAngle(const real& angle);

    static inline MultiScanMapper VelodyneVLP16() {return MultiScanMapper(-15.f, 15.f, 16);}
    static inline MultiScanMapper VelodyneHDL32() {return MultiScanMapper(-30.67f, 10.67f, 32);}
    static inline MultiScanMapper VelodyneHDL64() {return MultiScanMapper(-24.9f, 2.f, 64);}

  private:
    real _lower_bound;       // vertical angle of first scan ring
    real _upper_bound;       // vertical angle of last scan ring
    size_t _num_scan_rings;  // number of scan rings
    real _factor;            // linear interpolation factor

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace srrg_loam */
