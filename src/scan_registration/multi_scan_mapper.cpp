#include <scan_registration/multi_scan_mapper.h>

namespace srrg_loam {

  MultiScanMapper::MultiScanMapper(const real& lower_bound_,
                                   const real& upper_bound,
                                   const size_t& num_scan_rings_) : _lower_bound(lower_bound_),
                                                                    _upper_bound(upper_bound),
                                                                    _num_scan_rings(num_scan_rings_){
    _factor = ((_num_scan_rings - 1) / (_upper_bound - _lower_bound));
  }

  MultiScanMapper::~MultiScanMapper() {
    std::cerr << "[MultiScanMapper]: destroyed." << std::endl;
  }

  int MultiScanMapper::getRingForAngle(const real& angle) {
    return int(((angle * 180 / M_PI) - _lower_bound) * _factor + 0.5);
  }


} /* namespace srrg_loam */
