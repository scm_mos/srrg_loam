#include <scan_registration/detector.h>
#include <utils/scan_utils.h>
#include <pcl/filters/voxel_grid.h>

namespace srrg_loam {

  Detector::Detector() {
    _is_init = false;
  }

  Detector::~Detector() {
    std::cerr << "[Detector]: destroyed." << std::endl;
  }

  void Detector::setup() {
    if(!_corner_points_sharp || !_corner_points_less_sharp ||
        !_surface_points_flat || !_surface_points_less_flat)
      throw std::runtime_error("[Detector][setup]: missing corner and surface clouds.");
    _is_init = true;
  }

  void Detector::clearClouds() {
    _corner_points_sharp->clear();
    _corner_points_less_sharp->clear();
    _surface_points_flat->clear();
    _surface_points_less_flat->clear();
    _scan_indices.clear();
  }

  void Detector::collectFullResolutionCloud() {
    _laser_cloud.clear();
    size_t cloud_size = 0;
    for (size_t i = 0; i < _laser_scans->size(); ++i) {
      _laser_cloud += _laser_scans->at(i);

      IndexRange range(cloud_size, 0);
      cloud_size += _laser_scans->at(i).size();
      range.second = cloud_size > 0 ? cloud_size - 1 : 0;
      _scan_indices.push_back(range);
    }
  }


  void Detector::setRegionBuffersFor(const size_t& start_idx_, const size_t& end_idx_)
  {
    // resize buffers
    size_t region_size = end_idx_ - start_idx_ + 1;
    _region_curvature.resize(region_size);
    _region_sort_indices.resize(region_size);
    _region_label.assign(region_size, SURFACE_LESS_FLAT);

    // calculate point curvatures and reset sort indices
    float point_weight = -2 * _config.curvature_region;

    for (size_t i = start_idx_, region_idx = 0; i <= end_idx_; i++, region_idx++) {
      float diffX = point_weight * _laser_cloud[i].x;
      float diffY = point_weight * _laser_cloud[i].y;
      float diffZ = point_weight * _laser_cloud[i].z;

      for (int j = 1; j <= _config.curvature_region; j++) {
        diffX += _laser_cloud[i + j].x + _laser_cloud[i - j].x;
        diffY += _laser_cloud[i + j].y + _laser_cloud[i - j].y;
        diffZ += _laser_cloud[i + j].z + _laser_cloud[i - j].z;
      }

      _region_curvature[region_idx] = diffX * diffX + diffY * diffY + diffZ * diffZ;
      _region_sort_indices[region_idx] = i;
    }

    // sort point curvatures
    for (size_t i = 1; i < region_size; i++) {
      for (size_t j = i; j >= 1; j--) {
        if (_region_curvature[_region_sort_indices[j] - start_idx_] < _region_curvature[_region_sort_indices[j - 1] - start_idx_]) {
          std::swap(_region_sort_indices[j], _region_sort_indices[j - 1]);
        }
      }
    }
  }

  void Detector::setScanBuffersFor(const size_t& start_idx_, const size_t& end_idx_) {
    // resize buffers
    size_t scan_size = end_idx_ - start_idx_ + 1;
    _scan_neighbor_picked.assign(scan_size, 0);

    // mark unreliable points as picked
    for (size_t i = start_idx_ + _config.curvature_region; i < end_idx_ - _config.curvature_region; i++) {
      const pcl::PointXYZI& previous_point = (_laser_cloud[i - 1]);
      const pcl::PointXYZI& point = (_laser_cloud[i]);
      const pcl::PointXYZI& next_point = (_laser_cloud[i + 1]);

      real diff_next = calcSquaredDiff(next_point, point);

      if (diff_next > 0.1) {
        real depth1 = pointDistance(point);
        real depth2 = pointDistance(next_point);
        if (depth1 > depth2) {
          real weighted_distance = std::sqrt(calcSquaredDiff(next_point, point, depth2 / depth1)) / depth2;
          if (weighted_distance < 0.1) {
            std::fill_n(&_scan_neighbor_picked[i - start_idx_ - _config.curvature_region], _config.curvature_region + 1, 1);
            continue;
          }
        } else {
          real weighted_distance = std::sqrt(calcSquaredDiff(point, next_point, depth1 / depth2)) / depth1;
          if (weighted_distance < 0.1) {
            std::fill_n(&_scan_neighbor_picked[i - start_idx_ + 1], _config.curvature_region + 1, 1);
          }
        }
      }

      real diff_previous = calcSquaredDiff(point, previous_point);
      real dis = squaredPointDistance(point);

      if (diff_next > 0.0002 * dis && diff_previous > 0.0002 * dis) {
        _scan_neighbor_picked[i - start_idx_] = 1;
      }
    }

  }

  void Detector::markAsPicked(const size_t& cloud_idx_, const size_t& scan_idx_)
  {
    _scan_neighbor_picked[scan_idx_] = 1;

    for (size_t i = 1; i <= _config.curvature_region; ++i) {
      if (calcSquaredDiff(_laser_cloud[cloud_idx_ + i], _laser_cloud[cloud_idx_ + i - 1]) > 0.05) {
        break;
      }

      _scan_neighbor_picked[scan_idx_ + i] = 1;
    }

    for (size_t i = 1; i <= _config.curvature_region; ++i) {
      if (calcSquaredDiff(_laser_cloud[cloud_idx_ - i], _laser_cloud[cloud_idx_ - i + 1]) > 0.05) {
        break;
      }

      _scan_neighbor_picked[scan_idx_ - i] = 1;
    }
  }


  void Detector::extractFeatures() {
    // extract features from individual scans
    size_t scans_num = _scan_indices.size();
    for (size_t i = 0; i < scans_num; ++i) {
      CloudXYZI::Ptr surf_points_less_flat_scan = CloudXYZI::Ptr(new CloudXYZI());
      const size_t& scan_start_idx = _scan_indices[i].first;
      const size_t& scan_end_idx = _scan_indices[i].second;

      // skip empty scans
      if (scan_end_idx <= scan_start_idx + 2 * _config.curvature_region) {
        continue;
      }

      // reset scan buffers
      setScanBuffersFor(scan_start_idx, scan_end_idx);

      // extract features from equally sized scan regions
      for (size_t j = 0; j < _config.number_feature_region; ++j) {
        size_t sp = ((scan_start_idx + _config.curvature_region) * (_config.number_feature_region - j)
            + (scan_end_idx - _config.curvature_region) * j) / _config.number_feature_region;
        size_t ep = ((scan_start_idx + _config.curvature_region) * (_config.number_feature_region - 1 - j)
            + (scan_end_idx - _config.curvature_region) * (j + 1)) / _config.number_feature_region - 1;

        // skip empty regions
        if (ep <= sp) {
          continue;
        }

        size_t region_size = ep - sp + 1;

        // reset region buffers
        setRegionBuffersFor(sp, ep);


        // extract corner features
        int largestPickedNum = 0;
        for (size_t k = region_size; k > 0 && largestPickedNum < _config.max_corner_less_sharp;) {
          size_t idx = _region_sort_indices[--k];
          size_t scanIdx = idx - scan_start_idx;
          size_t regionIdx = idx - sp;

          if (_scan_neighbor_picked[scanIdx] == 0 &&
              _region_curvature[regionIdx] > _config.surface_curvature_threshold) {

            largestPickedNum++;
            if (largestPickedNum <= _config.max_corner_sharp) {
              _region_label[regionIdx] = CORNER_SHARP;
              _corner_points_sharp->push_back(_laser_cloud[idx]);
            } else {
              _region_label[regionIdx] = CORNER_LESS_SHARP;
            }
            _corner_points_less_sharp->push_back(_laser_cloud[idx]);

            markAsPicked(idx, scanIdx);
          }
        }

        // extract flat surface features
        int smallestPickedNum = 0;
        for (int k = 0; k < region_size && smallestPickedNum < _config.max_surface_flat; k++) {
          size_t idx = _region_sort_indices[k];
          size_t scanIdx = idx - scan_start_idx;
          size_t regionIdx = idx - sp;

          if (_scan_neighbor_picked[scanIdx] == 0 &&
              _region_curvature[regionIdx] < _config.surface_curvature_threshold) {

            smallestPickedNum++;
            _region_label[regionIdx] = SURFACE_FLAT;
            _surface_points_flat->push_back(_laser_cloud[idx]);

            markAsPicked(idx, scanIdx);
          }
        }

        // extract less flat surface features
        for (int k = 0; k < region_size; k++) {
          if (_region_label[k] <= SURFACE_LESS_FLAT) {
            surf_points_less_flat_scan->push_back(_laser_cloud[sp + k]);
          }
        }
      }

      // down size less flat surface point cloud of current scan
      pcl::PointCloud<pcl::PointXYZI> surf_points_less_flat_scanDS;
      pcl::VoxelGrid<pcl::PointXYZI> downsize_filter;
      downsize_filter.setInputCloud(surf_points_less_flat_scan);
      downsize_filter.setLeafSize(_config.less_flat_filter_size, _config.less_flat_filter_size, _config.less_flat_filter_size);
      downsize_filter.filter(surf_points_less_flat_scanDS);

      *_surface_points_less_flat += surf_points_less_flat_scanDS;
    }
  }

  void Detector::compute() {
    if(!_laser_scans)
      throw std::runtime_error("[Detector][compute]: missing laser scans cloud.");
    //bdc clear cloud and indices
    clearClouds();
    collectFullResolutionCloud();
    extractFeatures();
  }

} /* namespace srrg_loam */
