#pragma once
#include <parameters/parameters.h>
#include <types/imu_state.h>
#include "detector.h"
#include "multi_scan_mapper.h"

namespace srrg_loam {

  class ScanRegistration {
  public:
    ScanRegistration();
    ~ScanRegistration();
    
    inline RegistrationConfig& mutableConfig() {return _config;}
    inline const RegistrationConfig& config() const {return _config;}
    
    inline Detector& detector() {return _detector;}

    void setMultiScanMapper(MultiScanMapper* multi_scan_mapper_);

    void setScan(const Time& scan_time_, CloudXYZ* laser_cloud_);
    void setImuData(const Time& imu_time_, const ImuState& imu_state_);

    void setup();
    void compute();
        
  protected:
    void projectPointToStartOfSweep(pcl::PointXYZI& point_, const real& relative_time_);
    void processScanLines();

  private:
    RegistrationConfig _config;
    Detector _detector;

    CloudXYZIVector* _laser_cloud_scans;
    
    Time _scan_time;
    CloudXYZ* _laser_cloud = 0;
    MultiScanMapper* _multi_scan_mapper = 0;

    Time _imu_time;
    ImuState _imu_state;
    
    bool _is_init;
    
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };
  
}
