# Laser Odometry Node
#
add_executable(laser_odometry_node
  laser_odometry_node.cpp
)

target_link_libraries(laser_odometry_node
  srrg_loam_laser_odometry_lib
  ${catkin_LIBRARIES}
)


# Laser Mapping Node
#
add_executable(laser_mapping_node
  laser_mapping_node.cpp
)

target_link_libraries(laser_mapping_node
  srrg_loam_laser_mapping_lib
  ${catkin_LIBRARIES}
)


# Multi Scan Registration Node
#
add_executable(multi_scan_registration_node
  multi_scan_registration_node.cpp
)

target_link_libraries(multi_scan_registration_node
  srrg_loam_scan_registration_lib
  ${catkin_LIBRARIES}
)

#Transform Mantainance Node
#
add_executable(transform_maintenance_node
  transform_maintenance_node.cpp
)

target_link_libraries(transform_maintenance_node
  srrg_loam_transform_maintenance_lib
  ${catkin_LIBRARIES}
)
