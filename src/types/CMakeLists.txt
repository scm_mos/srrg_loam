 add_library(srrg_loam_types_lib SHARED
   imu_state.cpp
   )

target_link_libraries(srrg_loam_types_lib
   ${catkin_LIBRARIES}
   )
