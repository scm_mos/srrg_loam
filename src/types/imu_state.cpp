#include "imu_state.h"

namespace srrg_loam {
  
  ImuState::ImuState() {
    orientation  = Vector3::Zero();
    position     = Vector3::Zero();
    velocity     = Vector3::Zero();
    acceleration = Vector3::Zero();
  }

  void ImuState::interpolate(const ImuState& start_,
                             const ImuState& end_,
                             const real& ratio_,
                             ImuState& result_) {
    
    const real inv_ratio = 1.f - ratio_;
    if(inv_ratio < 0.f)
      throw std::runtime_error("[ImuState][interpolate]: provided ratio is outside the range [0;1]");

    const real& start_roll  = start_.orientation(0);
    const real& start_pitch = start_.orientation(1);
    const real& start_yaw   = start_.orientation(2);
    const real& end_roll    = end_.orientation(0);
    const real& end_pitch   = end_.orientation(1);
    const real& end_yaw     = end_.orientation(2);
    real& result_roll       = result_.orientation(0);
    real& result_pitch      = result_.orientation(1);
    real& result_yaw        = result_.orientation(2);
    
    //interpolate position
    result_.position = start_.position * inv_ratio + end_.position * ratio_;
    //interpolate velocity
    result_.velocity = start_.velocity * inv_ratio + end_.velocity * ratio_;
    //interpolate roll
    result_roll      = start_roll * inv_ratio + end_roll * ratio_;
    //interpolate pitch
    result_pitch     = start_pitch * inv_ratio + end_pitch * ratio_;    
    //interpolate yaw
    const real yaw_difference = start_yaw - end_yaw;
    if(yaw_difference > M_PI)
      result_yaw = start_yaw * inv_ratio + (end_yaw + 2*M_PI) * ratio_;
    else if(yaw_difference < -M_PI)
      result_yaw = start_yaw * inv_ratio + (end_yaw - 2*M_PI) * ratio_;
    else
      result_yaw = start_yaw * inv_ratio + end_yaw * ratio_;

    //bdc TODO acceleration is not used?
  }
    
  
}
