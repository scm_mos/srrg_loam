#pragma once
#include <scan_registration/scan_registration.h>

namespace srrg_loam {

  class System {
  public:
    System();
    virtual ~System();

  private:

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  };

} /* namespace srrg_loam */

