add_library(srrg_loam_laser_mapping_lib SHARED
  BasicLaserMapping.cpp
  LaserMapping.cpp
)

target_link_libraries(srrg_loam_laser_mapping_lib
  ${catkin_LIBRARIES}
  ${PCL_LIBRARIES})
