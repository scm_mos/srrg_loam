# SRRG_LOAM

SRRG porting (catkin-ized) of loam_velodyne from github repo of [laboshinl](https://github.com/laboshinl/loam_velodyne).

**Mantainers of this repo:** [Bartolomeo Della Corte](http://bartdc.gitlab.io/)

**LOAM_VELODYNE Author:** [Ji Zhang](https://scholar.google.com/citations?user=h-k8LTIAAAAJ&hl=en), [Sanjiv Singh](https://scholar.google.com/citations?user=IxCZDBQAAAAJ&hl=en)

## Dependencies

* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)

## Build me

* put me in you catkin workspace, containing the listed dependencies
* `catkin build / catkin_make`

## Test on KITTI dataset
First of all, credits to [thomas789](https://github.com/tomas789/kitti2bag) for the `kitti2bag` conversion tool.

* download a KITTI bag from the following list:

| Sequence | Date | Drive |   link      |
|----------|----|-----|-----------|
|    00    |2011-10-03|0027|[Download](https://drive.google.com/file/d/1qDyr6BfnZYAxkmlC5QgCd-IbhJZyLlNI/view?usp=sharing)|
|    ...   |  -   |    -  |     -       |
|    06    |2011-09-30|0020|[Download](https://drive.google.com/file/d/1rweYca3oZTf9f9VcQ4oP78tM0SMBMjXG/view?usp=sharing)|
|    ...   |  -   |    -  |     -       |
|    10    |2011-09-30|0034|[Download](https://drive.google.com/open?id=1xgpwrrpobzFX4rwolxbOQ61EV0h6XTKK)|

* launch the node using the kitti launcher: `roslaunch srrg_loam kitti.launch`

* play the bag

## Use me

* create your own `launch` file. You can choose btw different velodyne(s)

* `roslaunch srrg_loam <your/launch/file>.launch`

## License

LOAM_VELODYNE is released under a [2-Clause BSD license](https://github.com/laboshinl/loam_velodyne/blob/master/LICENSE).

If you use this software in academic work, please cite the relevant [LOAM](https://www.ri.cmu.edu/pub_files/2014/7/Ji_LidarMapping_RSS2014_v8.pdf) paper.

## TODO

- [] separate base lib from ROS nodes
- [] txtio/boss apps
- [] ...
