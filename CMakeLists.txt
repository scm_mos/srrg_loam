cmake_minimum_required(VERSION 2.8.3)
project(srrg_loam)

#bdc cmake_modules
find_package(srrg_cmake_modules REQUIRED)
set(CMAKE_MODULE_PATH ${srrg_cmake_modules_INCLUDE_DIRS})

#bdc compile flags
set(CMAKE_BUILD_TYPE RELEASE)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -fPIC --std=c++14 -march=native")
message("${PROJECT_NAME}|build type: [ ${CMAKE_BUILD_TYPE} ]")
message("${PROJECT_NAME}|build flags: [${CMAKE_CXX_FLAGS} ]")

#bdc find damn packages
find_package(Eigen3 REQUIRED)

find_package(OpenCV REQUIRED)
message("${PROJECT_NAME}|found OpenCV version: '${OpenCV_VERSION}' (${OpenCV_DIR})")

find_package(PCL REQUIRED)

#bdc find the other damn required catkin packages 
find_package(catkin REQUIRED COMPONENTS
  srrg_core
  geometry_msgs
  nav_msgs
  sensor_msgs
  roscpp
  rospy
  std_msgs
  tf
  pcl_conversions
)

#bdc include damn directories
include_directories(
  ${EIGEN3_INCLUDE_DIR}
  ${OpenCV_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
  ${PROJECT_SOURCE_DIR}/src)
  
#ds help the catkin tool on 16.04 (cmake seems unable to find single libraries, although catkin claims the link_directories call is not required)
#ds in order to avoid linking against the catkin_LIBRARIES bulk everytime enable this so one can select single libraries
link_directories(${catkin_LIBRARY_DIRS})
  
#bdc create a damn catkin package
catkin_package(
  INCLUDE_DIRS  src
  LIBRARIES
  srrg_loam_scan_registration_lib
  srrg_loam_transform_maintenance_lib
  srrg_loam_laser_odometry_lib
  srrg_loam_laser_mapping_lib  
  CATKIN_DEPENDS  srrg_core geometry_msgs nav_msgs roscpp rospy std_msgs tf pcl_conversions
)

add_subdirectory(src)
